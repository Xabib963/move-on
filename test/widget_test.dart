// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:moveon/data/model/Order.dart';

import 'package:moveon/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Future<Order> addOrder({Map? newOrdere}) async {
    //   var body = jsonEncode({
    //     'typeOfjourny': 'jh',
    //     'phoneNumber': 'zxc',
    //     'Weight': 'zxc',
    //     'fullName': 'zxc',
    //     'Address': 'zxc',
    //     'Slatitude': 'zxc',
    //     'Slongitude': 'zxc',
    //     'dlatitude': 'zxc',
    //     'dlongitude': 'zxc',
    //   });
    //   var response = await http.post(
    //       Uri.parse(
    //           'https://move-on-ea195-default-rtdb.europe-west1.firebasedatabase.app/orders.json'),
    //       headers: {'Content-Type': 'application/json'},
    //       body: body);

    //   // if (response.statusCode == 200) {
    //   //   var results = jsonDecode(response.body);
    //   //   print(Order.fromJson(results));
    //   //   return Order.fromJson(results);
    //   // } else {
    //   //   throw Exception();
    //   // }
    // }

    await tester.pumpWidget(MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });
}
