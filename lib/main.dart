import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'view/AuthScreen/pages/LoginScreen.dart';

import 'app/controller/AuthController.dart';
import 'app/settings/Applocal.dart';
import 'app/settings/Routes.dart';
import 'app/settings/bindings/initialBindings.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  // AuthController _authController = Get.find();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Move On',
      theme: ThemeData(
        canvasColor: const Color.fromRGBO(106, 91, 226, 1),
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.indigo,
        ),
        useMaterial3: false,
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      fallbackLocale: Locale('en', ''),
      locale: Get.deviceLocale,
      translationsKeys: AppTranslation.translationsKeys,
      getPages: appRoutes(),
      initialBinding: InitialBindings(),
      initialRoute: LoginsScreen.routeName,
    );
  }
}
