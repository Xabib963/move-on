import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:moveon/view/AuthScreen/pages/Addinformation.dart';

import '../../../app/controller/AuthController.dart';
import '../../../app/controller/USerController.dart';
import '../../../app/settings/Themes.dart';

class GetStarted extends StatelessWidget {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  static const routeName = '/login';
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.95),
              borderRadius: BorderRadius.circular(20)),
          width: media(context).width * 0.85,
          height: media(context).height * 0.6,
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                child: SizedBox(
                  width: media(context).width * 0.85,
                  height: media(context).height * 0.6,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GetBuilder<AuthController>(builder: (controller) {
                        return controller.sighnUp
                            ? const Text(
                                overflow: TextOverflow.clip,
                                'Sign Up',
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 35,
                                    color: Colors.indigo,
                                    fontWeight: FontWeight.bold),
                              )
                            : const Text(
                                overflow: TextOverflow.clip,
                                'Log In',
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 35,
                                    color: Colors.indigo,
                                    fontWeight: FontWeight.bold),
                              );
                      }),
                      Form(
                        key: formKey,
                        child: SizedBox(
                          height: media(context).height * 0.23,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0),
                                child: SizedBox(
                                  height: media(context).height * 0.11,
                                  child: TextFormField(
                                    validator: (value) {
                                      if (!(value!.contains('@')) ||
                                          !(value.contains('.com'))) {
                                        return "Not a Valid Email Format";
                                      }
                                      return null;
                                    },
                                    controller: _emailController,
                                    decoration: const InputDecoration(
                                        label: Text('Email'),
                                        border: OutlineInputBorder()),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0),
                                child: SizedBox(
                                  height: media(context).height * 0.07,
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value!.length < 6) {
                                        return "weak pass";
                                      }
                                      return null;
                                    },
                                    controller: _passwordController,
                                    decoration: const InputDecoration(
                                        label: Text('Password'),
                                        border: OutlineInputBorder()),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      GetBuilder<AuthController>(builder: (_) {
                        return AuthController.instance.loading
                            ? const SpinKitDualRing(
                                color: Colors.indigo,
                              )
                            : AuthController.instance.sighnUp
                                ? ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        elevation: 10,
                                        fixedSize: Size(
                                            media(context).width * 0.6,
                                            media(context).width * 0.15),
                                        disabledBackgroundColor:
                                            Colors.transparent,
                                        backgroundColor:
                                            Colors.indigo.withOpacity(0.8)),
                                    onPressed: () async {
                                      if (formKey.currentState!.validate()) {
                                        Get.toNamed(AdduserInfo.routeName,
                                            arguments: {
                                              'email':
                                                  _emailController.text.trim(),
                                              'password': _passwordController
                                                  .text
                                                  .trim()
                                            });
                                      }
                                    },
                                    child: const Text(
                                      overflow: TextOverflow.clip,
                                      'Sign Up',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Poppins',
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ))
                                : ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        elevation: 10,
                                        fixedSize: Size(
                                            media(context).width * 0.6,
                                            media(context).width * 0.15),
                                        disabledBackgroundColor:
                                            Colors.transparent,
                                        backgroundColor:
                                            Colors.indigo.withOpacity(0.8)),
                                    onPressed: () async {
                                      if (formKey.currentState!.validate()) {
                                        try {
                                          AuthController.instance.logupdate();
                                          // AuthController.instance.loginwithemail(
                                          //     email: _emailController
                                          //         .text
                                          //         .trim(),
                                          //     password:
                                          //         _passwordController
                                          //             .text
                                          //             .trim());
                                          await AuthController.instance.login(
                                              email: _emailController.text
                                                  .trim()
                                                  .toLowerCase(),
                                              password: _passwordController.text
                                                  .trim());

                                          AuthController.instance.navigate();
                                        } on HttpException catch (error) {
                                          if (error
                                              .toString()
                                              .contains("EMAIL_NOT_FOUND")) {
                                            Get.snackbar(
                                                "EMAIL NOT FOUND", "Sorry");
                                          }
                                          if (error
                                              .toString()
                                              .contains("INVALID_PASSWORD")) {
                                            Get.snackbar(
                                                "INVALID PASSWORD", "Sorry");
                                          }
                                          if (error.toString().contains(
                                                  "INVALID_PASSWORD") ||
                                              error.toString().contains(
                                                  "EMAIL_NOT_FOUND")) {
                                            Get.snackbar(
                                                "INVALID Infos", "Sorry");
                                          }

                                          if (error
                                              .toString()
                                              .contains("USER_DISABLED")) {
                                            Get.snackbar(
                                                "USER DISABLED", "Sorry");
                                          }
                                        }
                                        AuthController.instance.logupdate();
                                      }
                                    },
                                    child: const Text(
                                      overflow: TextOverflow.clip,
                                      'Log In',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Poppins',
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ));
                      }),
                      const Row(
                        children: [
                          Expanded(child: Divider(thickness: 2)),
                          Text(
                            overflow: TextOverflow.clip,
                            'or',
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 20,
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                          ),
                          Expanded(
                              child: Divider(
                            thickness: 2,
                          )),
                        ],
                      ),
                      GetBuilder<AuthController>(builder: (control) {
                        return control.sighnUp
                            ? TextButton(
                                onPressed: () {
                                  control.changToSignUp();
                                },
                                child: const Text(
                                  overflow: TextOverflow.clip,
                                  'Login instade',
                                  style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 20,
                                      color: Colors.indigoAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            : TextButton(
                                onPressed: () {
                                  control.changToSignUp();
                                },
                                child: const Text(
                                  overflow: TextOverflow.clip,
                                  'Register for Free',
                                  style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 20,
                                      color: Colors.indigoAccent,
                                      fontWeight: FontWeight.bold),
                                ),
                              );
                      }),
                      const SizedBox(
                        height: 30,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
