import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../../app/settings/Themes.dart';
import '../../../main.dart';
import '../widgets/GetStarted.dart';

class LoginsScreen extends StatelessWidget {
  LoginsScreen({super.key});

  static const routeName = '/login';
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            width: media(context).width,
            bottom: 0,
            child: Lottie.asset(
              'assets/26873-moving.json',
            ),
          ),
          Positioned(
              width: media(context).width * 0.8,
              top: media(context).height * 0.15,
              left: 40,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: media(context).width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        maxLines: 2,
                        'Move On',
                        style: textStyle(
                            size: 25,
                            color: Colors.grey[600]!,
                            weight: FontWeight.bold),
                      ),
                    ),
                  ),
                  const Divider(thickness: 2),
                  SizedBox(
                    width: media(context).width,
                    child: Text(
                      maxLines: 5,
                      'leading'.tr,
                      style: textStyle(
                          size: media(context).width * 0.07,
                          color: Colors.grey,
                          weight: FontWeight.normal),
                    ),
                  ),
                ],
              )),
          Positioned(
              width: media(context).width * 0.8,
              top: media(context).height * 0.55,
              left: 40,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 10,
                    fixedSize: Size(media(context).width * 0.7,
                        media(context).width * 0.15),
                    disabledBackgroundColor: Colors.transparent,
                    backgroundColor: Colors.indigo.withOpacity(0.8)),
                onPressed: () {
                  showDialog(
                      barrierDismissible: true,
                      barrierLabel: "hi",
                      context: context,
                      builder: (
                        context,
                      ) =>
                          Scaffold(
                              backgroundColor: Colors.transparent,
                              body: GetStarted()));
                },
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      overflow: TextOverflow.clip,
                      'Get Started',
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Icon(
                      CupertinoIcons.arrow_right_circle,
                      color: Colors.white,
                    )
                  ],
                ),
              )),
        ],
      ),
    );
  }
}
