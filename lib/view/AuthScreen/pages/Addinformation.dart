import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app/controller/AuthController.dart';
import '../../../app/controller/USerController.dart';
import '../../../app/settings/Themes.dart';
import '../../widgets/customInput.dart';

class AdduserInfo extends StatelessWidget {
  AdduserInfo({super.key});
  static const routeName = '/AddUserinfo';
  Map<String, dynamic> email_pass = Get.arguments;

  UserController _userController = Get.find();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          'I N F O',
          style:
              textStyle(size: 20, color: Colors.white, weight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                'Choose your Avatar',
                style: textStyle(
                    size: 25, color: Colors.black45, weight: FontWeight.normal),
              ),
            ),
            GetBuilder<UserController>(builder: (_) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () {
                      _userController.updateIndex(1, 'batman.png');
                    },
                    child: CircleAvatar(
                      backgroundColor: _userController.index == 1
                          ? Colors.greenAccent
                          : Colors.white,
                      backgroundImage: AssetImage('assets/batman.png'),
                      radius: 40,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _userController.updateIndex(2, 'old_man.png');
                    },
                    child: CircleAvatar(
                      backgroundColor: _userController.index == 2
                          ? Colors.greenAccent
                          : Colors.white,
                      backgroundImage: AssetImage('assets/old_man.png'),
                      radius: 40,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _userController.updateIndex(3, 'robo.png');
                    },
                    child: CircleAvatar(
                      backgroundColor: _userController.index == 3
                          ? Colors.greenAccent
                          : Colors.white,
                      backgroundImage: AssetImage('assets/robo.png'),
                      radius: 40,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _userController.updateIndex(4, 'gym.png');
                    },
                    child: CircleAvatar(
                      backgroundColor: _userController.index == 4
                          ? Colors.greenAccent
                          : Colors.white,
                      backgroundImage: AssetImage('assets/gym.png'),
                      radius: 40,
                    ),
                  )
                ],
              );
            }),
            Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: CustomInput.user(
                            controller: _userController,
                            hint: 'Full Name',
                            title: 'Name')),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: CustomInput.user(
                            controller: _userController,
                            hint: 'Adress',
                            title: 'address')),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: CustomInput.user(
                            controller: _userController,
                            hint: 'Age',
                            title: 'age')),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: CustomInput.user(
                            controller: _userController,
                            hint: 'job title',
                            title: 'job')),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        child: CustomInput.user(
                            controller: _userController,
                            hint: 'Phone Number',
                            title: 'phoneNumber')),
                  ],
                ),
              ),
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 10,
                    fixedSize: Size(media(context).width * 0.6,
                        media(context).width * 0.15),
                    disabledBackgroundColor: Colors.transparent,
                    backgroundColor: Colors.indigo.withOpacity(0.8)),
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    try {
                      await AuthController.instance.register(
                          email: email_pass['email'].trim().toLowerCase(),
                          password: email_pass['password'].trim());
                      try {
                        await AuthController.instance.addUser(newUser: {
                          "userId": AuthController.instance.userId,
                          'email': email_pass['email'],
                          'Name': _userController.newUser["Name"],
                          'address': _userController.newUser["address"],
                          'age': _userController.newUser["age"],
                          'job': _userController.newUser["job"],
                          'number': _userController.newUser["number"],
                          'avatar': _userController.avatar == ''
                              ? 'batman.png'
                              : _userController.avatar,
                          'role': 'empl'
                        });
                        Get.snackbar("Add Succefully", "Thnx");
                      } catch (e) {
                        print(e);
                      }

                      UserController.instance.assignUser();
                      AuthController.instance.logupdate();
                    } on HttpException catch (error) {
                      if (error.toString().contains("EMAIL_EXISTS")) {
                        Get.snackbar("EMAIL_EXISTS", "Sorry");
                      }
                      if (error.toString().contains("OPERATION_NOT_ALLOWED")) {
                        Get.snackbar("OPERATION NOT ALLOWED", "Sorry");
                      }
                      if (error
                          .toString()
                          .contains("TOO_MANY_ATTEMPTS_TRY_LATER")) {
                        Get.snackbar("TOO MANY ATTEMPTS TRY LATER", "Sorry");
                      }
                    } catch (e) {
                      throw Exception(e);
                    }
                    // AuthController.instance.register(
                    //     email: email_pass['email'].trim(),
                    //     password: email_pass['pass'].trim());
                  }
                },
                child: const Text(
                  overflow: TextOverflow.clip,
                  'Sign Up',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Poppins',
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ))
          ]),
        ),
      ),
    );
  }
}
