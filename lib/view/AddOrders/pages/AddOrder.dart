import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../app/controller/AddOrderController.dart';
import '../../../app/settings/Themes.dart';
import '../../widgets/CustomAppBar.dart';
import '../../widgets/customInput.dart';
import 'MapScreen.dart';

class AddOrdersScreen extends StatelessWidget {
  AddOrdersScreen({super.key});
  static const routeName = '/addOrder';
  String type = Get.arguments;
  AddOrderController _addOrderController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'Add Order'.tr, allow: true),
      body: SafeArea(
          child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50))),
        margin: const EdgeInsets.symmetric(vertical: 30, horizontal: 10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Divider(
                  endIndent: media(context).width * 0.3,
                  color: Colors.black38,
                  thickness: 3,
                  indent: media(context).width * 0.3,
                ),
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: CustomInput(
                    hint: 'Full Name'.tr,
                    title: 'fullName',
                    addOrderController: _addOrderController,
                  )),
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: CustomInput(
                    hint: 'Address'.tr,
                    title: 'Address',
                    addOrderController: _addOrderController,
                  )),
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: CustomInput(
                    hint: 'per bound'.tr,
                    title: 'Weight',
                    addOrderController: _addOrderController,
                  )),
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GetBuilder<AddOrderController>(builder: (c) {
                        return DropdownButton(
                          style: textStyle(
                              weight: FontWeight.bold,
                              color: Colors.black54,
                              size: media(context).width * 0.03),
                          value: _addOrderController.journy,
                          alignment: AlignmentDirectional.center,
                          hint: Text(
                            'Choose journy'.tr,
                          ),
                          items: [
                            DropdownMenuItem(
                              value: 'internationaly',
                              child: Text('internationly'.tr,
                                  style: textStyle(
                                      weight: FontWeight.bold,
                                      color: Colors.black54,
                                      size: media(context).width * 0.03),
                                  textAlign: TextAlign.center),
                            ),
                            DropdownMenuItem(
                              value: 'localy',
                              child: Text('localy'.tr,
                                  style: textStyle(
                                      weight: FontWeight.bold,
                                      color: Colors.black54,
                                      size: media(context).width * 0.03),
                                  textAlign: TextAlign.center),
                            ),
                          ],
                          onChanged: (value) {
                            _addOrderController.updateJourny(value);
                            _addOrderController.newOrder.update(
                              'typeOfjourny',
                              (_) => value,
                              ifAbsent: () => 'typeOfjourny',
                            );
                          },
                        );
                      }),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GetBuilder<AddOrderController>(builder: (_) {
                            return SizedBox(
                              width: media(context).width * 0.3,
                              child: FittedBox(
                                child: Text(_addOrderController.date,
                                    style: textStyle(
                                      size: 20,
                                      color: Colors.grey[600]!,
                                      weight: FontWeight.bold,
                                    )),
                              ),
                            );
                          }),
                          IconButton(
                              onPressed: () async {
                                _addOrderController.chooseDate(context);
                                print(_addOrderController.date);
                                await _addOrderController.newOrder.update(
                                  'date',
                                  (_) => _addOrderController.date,
                                  ifAbsent: () => 'date',
                                );
                                print(_addOrderController.newOrder['date']);
                              },
                              icon: const Icon(
                                Icons.date_range,
                                color: Colors.indigo,
                              )),
                        ],
                      ),
                    ],
                  )),
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: CustomInput(
                    hint: 'Phone Number'.tr,
                    title: 'phoneNumber',
                    addOrderController: _addOrderController,
                  )),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'd&s'.tr,
                  style: textStyle(
                      size: media(context).width * 0.035,
                      color: Colors.grey[900]!,
                      weight: FontWeight.bold),
                ),
              ),
              InkWell(
                onTap: () {
                  Get.toNamed(MapScreen.routeName,
                      arguments: _addOrderController);
                },
                child: Container(
                    padding: EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 30),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.indigo,
                        borderRadius: BorderRadius.circular(15)),
                    width: media(context).width * 0.4,
                    height: media(context).width * 0.12,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        "Map".tr,
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge!
                            .copyWith(color: Colors.white, fontSize: 30),
                      ),
                    )),
              )
            ],
          ),
        ),
      )),
    );
  }
}
