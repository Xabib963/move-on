import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:moveon/app/controller/AddOrderController.dart';
import 'package:moveon/app/controller/AuthController.dart';

import '../../../app/controller/MapsController.dart';

import '../../../app/settings/Themes.dart';

class MapScreen extends StatelessWidget {
  MapScreen({super.key});
  static const routeName = '/Maps';
  MapsController _controller = Get.find();
  AddOrderController addOrderController = Get.arguments;

  static const CameraPosition _kGooglePlex = CameraPosition(
    bearing: 30,
    tilt: 0,
    target: LatLng(25.20200130866778, 55.269876569509506),
    zoom: 10.4746,
  );

  Completer<GoogleMapController> _mapController = Completer();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      floatingActionButton: InkWell(
        onTap: () async {
          print(addOrderController.newOrder);
          if (addOrderController.newOrder['Slongitude'] == null ||
              addOrderController.newOrder['Slongitude'] == null ||
              addOrderController.newOrder['dlongitude'] == null ||
              addOrderController.newOrder['phoneNumber'] == null ||
              addOrderController.newOrder['fullName'] == null) {
            Get.snackbar(
                "Please Fill all fields", "make sure all fileds are enterd");
          } else {
            await addOrderController.addOrder(newOrdere: {
              'creatorId': AuthController.instance.userId,
              'status': 'Pinding',
              'typeOfjourny':
                  addOrderController.newOrder['typeOfjourny'] ?? "localy",
              'phoneNumber':
                  addOrderController.newOrder['phoneNumber'].toString(),
              'Weight': addOrderController.newOrder['Weight'].toString(),
              'fullName': addOrderController.newOrder['fullName'].toString(),
              'Address': addOrderController.newOrder['Address'].toString(),
              'Slatitude': addOrderController.newOrder['Slatitude'].toString(),
              'Slongitude':
                  addOrderController.newOrder['Slongitude'].toString(),
              'dlatitude': addOrderController.newOrder['dlatitude'].toString(),
              'dlongitude':
                  addOrderController.newOrder['dlongitude'].toString(),
              'date': addOrderController.newOrder['date'] == null ||
                      addOrderController.newOrder['date'] == 'date'
                  ? "${DateTime.now()}"
                  : "${DateTime.now()}",
              'role': 'empl',
              "stuffNumber": 'none'
            }).then((value) =>
                Get.snackbar("added Sucsesfully", "added Sucsesfully"));
          }
        },
        child: Container(
            padding: EdgeInsets.all(10),
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.indigo, borderRadius: BorderRadius.circular(15)),
            width: media(context).width * 0.2,
            height: media(context).width * 0.12,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Text(
                "save".tr,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge!
                    .copyWith(color: Colors.white, fontSize: 30),
              ),
            )),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: IconButton(
          icon: Get.locale!.languageCode == 'ar'
              ? const Icon(Icons.arrow_back_ios, color: Colors.white)
              : const Icon(Icons.arrow_back_ios_new, color: Colors.white),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: Colors.transparent,
        title: Text(
          'map'.tr,
          style: textStyle(
              size: 20, color: Colors.white!, weight: FontWeight.bold),
        ),
      ),
      body: GetBuilder<MapsController>(builder: (s) {
        return Stack(
          children: [
            Center(
              child: GoogleMap(
                  onMapCreated: (controller) async {
                    _mapController.complete(controller);
                    await controller.setMapStyle(_controller.mapStyle);
                  },
                  // myLocationEnabled: true,
                  compassEnabled: true,
                  tiltGesturesEnabled: false,
                  markers: {
                    if (_controller.origin != null) _controller.origin!,
                    if (_controller.destination != null)
                      _controller.destination!
                  },
                  polylines: {
                    if (_controller.info != null)
                      Polyline(
                          polylineId: PolylineId("route"),
                          points: _controller.info!.polylinePoints
                              .map((e) => LatLng(e.latitude, e.longitude))
                              .toList(),
                          color: Colors.indigo)
                  },
                  onTap: (LatLng location) async {
                    if (_controller.origin == null ||
                        (_controller.origin != null &&
                            _controller.destination != null)) {
                      addOrderController.newOrder.update(
                        'Slatitude',
                        (_) => location.latitude,
                        ifAbsent: () => 'Slatitude',
                      );
                      addOrderController.newOrder.update(
                        'Slongitude',
                        (_) => location.longitude,
                        ifAbsent: () => 'Slongitude',
                      );
                      _controller.destination = null;
                      _controller.info = null;
                      await _controller.setMarker(
                          Marker(
                              markerId: const MarkerId("Origin"),
                              infoWindow: const InfoWindow(title: "Origin"),
                              icon: BitmapDescriptor.defaultMarkerWithHue(
                                  BitmapDescriptor.hueBlue),
                              position: location),
                          true);
                    } else {
                      addOrderController.newOrder.update(
                        'dlatitude',
                        (_) => location.latitude,
                        ifAbsent: () => 'dlatitude',
                      );
                      addOrderController.newOrder.update(
                        'dlongitude',
                        (_) => location.longitude,
                        ifAbsent: () => 'dlongitude',
                      );
                      await _controller.setMarker(
                          Marker(
                              position: location,
                              markerId: const MarkerId("destination"),
                              infoWindow:
                                  const InfoWindow(title: "Destination"),
                              icon: BitmapDescriptor.defaultMarkerWithHue(
                                  BitmapDescriptor.hueAzure)),
                          false);
                      // await _controller.setDirections();
                    }
                  },
                  mapType: MapType.normal,
                  initialCameraPosition: _kGooglePlex),
            ),
          ],
        );
      }),
    );
  }
}
    //  GetBuilder<MapController>(builder: (c) {
    //           return AnimatedPositioned(
    //               left: 0,
    //               right: 0,
    //               bottom: _controller.pinbill,
    //               duration: const Duration(milliseconds: 500),
    //               child: Align(
    //                 alignment: Alignment.center,
    //                 child: Container(
    //                   height: 90,
    //                   width: media(context).width * 0.9,
    //                   decoration: BoxDecoration(
    //                       color: Colors.white,
    //                       borderRadius: BorderRadius.circular(25)),
    //                   margin: const EdgeInsets.only(left: 20, right: 20),
    //                   padding: const EdgeInsets.all(5),
    //                   child: Row(children: [
    //                     CircleAvatar(
    //                       radius: 50,
    //                       backgroundImage: NetworkImage(
    //                         rest.mainPhotoSrc!,
    //                       ),
    //                       onBackgroundImageError: (exception, stackTrace) =>
    //                           const Center(
    //                               child: Icon(
    //                         Icons.error_outline_outlined,
    //                         color: Colors.red,
    //                       )),
    //                     ),
    //                     Column(
    //                       mainAxisAlignment: MainAxisAlignment.center,
    //                       children: [
    //                         SizedBox(
    //                           width: media(context).width * 0.45,
    //                           child: RichText(
    //                               maxLines: 2,
    //                               overflow: TextOverflow.ellipsis,
    //                               text: TextSpan(children: [
    //                                 TextSpan(
    //                                   text: '${rest.name!}',
    //                                   style: texty1(
    //                                       size: 18.1,
    //                                       weight: FontWeight.w700,
    //                                       color: Colors.indigo),
    //                                 ),
    //                               ])),
    //                         ),
    //                         SizedBox(
    //                           width: media(context).width * 0.45,
    //                           child: FittedBox(
    //                             child: RichText(
    //                                 maxLines: 1,
    //                                 overflow: TextOverflow.ellipsis,
    //                                 text: TextSpan(children: [
    //                                   TextSpan(
    //                                     text: 'latitude :',
    //                                     style: texty1(
    //                                         size: 18.1,
    //                                         weight: FontWeight.w700,
    //                                         color: Colors.black54),
    //                                   ),
    //                                   TextSpan(
    //                                     text: '${rest.geo!.latitude}',
    //                                     style: texty1(
    //                                         size: 16.1,
    //                                         weight: FontWeight.w700,
    //                                         color: Colors.black54),
    //                                   ),
    //                                 ])),
    //                           ),
    //                         ),
    //                         SizedBox(
    //                           width: media(context).width * 0.45,
    //                           child: FittedBox(
    //                             child: RichText(
    //                                 maxLines: 1,
    //                                 overflow: TextOverflow.ellipsis,
    //                                 text: TextSpan(children: [
    //                                   TextSpan(
    //                                     text: 'longitude :',
    //                                     style: texty1(
    //                                         size: 18.1,
    //                                         weight: FontWeight.w700,
    //                                         color: Colors.black54),
    //                                   ),
    //                                   TextSpan(
    //                                     text: '${rest.geo!.longitude}',
    //                                     style: texty1(
    //                                         size: 16.1,
    //                                         weight: FontWeight.w700,
    //                                         color: Colors.black54),
    //                                   ),
    //                                 ])),
    //                           ),
    //                         ),
    //                       ],
    //                     ),
    //                     Image.asset(
    //                       'assets/1486164730-114_79711.png',
    //                       width: 60,
    //                       height: 70,
    //                     )
    //                   ]),
    //                 ),
    //               ));
    //         })