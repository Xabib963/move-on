import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../app/settings/Themes.dart';

class SurveyWidget extends StatelessWidget {
  const SurveyWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 15,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      margin: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
      color: const Color.fromRGBO(252, 183, 136, 1),
      child: InkWell(
        borderRadius: BorderRadius.circular(15),
        onTap: () => Get.snackbar('Sorry Survey not available right now', ""),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 5),
                    child: SizedBox(
                      width: media(context).width * 0.3,
                      child: FittedBox(
                        fit: BoxFit.contain,
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'recent'.tr,
                          style: textStyle(
                              size: 25,
                              color: Colors.white54,
                              weight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 5),
                    child: SizedBox(
                      width: media(context).width * 0.6,
                      child: FittedBox(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'survy'.tr,
                          style: textStyle(
                              size: 25,
                              color: Colors.white,
                              weight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(10.0),
              child: CircleAvatar(
                radius: 32,
                backgroundColor: Colors.orangeAccent,
                child: Icon(
                  Icons.takeout_dining_outlined,
                  color: Colors.white,
                  size: 40,
                ),
              ),
            )
          ]),
        ),
      ),
    );
  }
}
