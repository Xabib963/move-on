import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../../app/controller/languagesController.dart';
import '../../../app/settings/Themes.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  CustomAppBar({
    super.key,
    required this.title,
    required this.allow,
  });
  final String title;
  final bool allow;
  LanguageController _languageController = Get.put(LanguageController());
  @override
  Widget build(BuildContext context) {
    return AppBar(
        centerTitle: true,
        elevation: 0,
        leading: allow
            ? IconButton(
                icon: Get.locale!.languageCode == 'ar'
                    ? const Icon(Icons.arrow_back_ios, color: Colors.white)
                    : const Icon(Icons.arrow_back_ios_new, color: Colors.white),
                onPressed: () {
                  Get.back();
                },
              )
            : const SizedBox(),
        backgroundColor: Colors.transparent,
        title: Text(
          title,
          style: textStyle(
              size: 20, color: Colors.white!, weight: FontWeight.bold),
        ),
        actions: []);
  }

  @override
  Size get preferredSize => Size(100, 60);
}
//  [
//               Container(
//                 margin: EdgeInsets.all(10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(15),
//                   color: Get.locale!.languageCode == 'ar'
//                       ? Colors.amber
//                       : Colors.transparent,
//                 ),
//                 child: TextButton(
//                     onPressed: () {
//                       _languageController.changeLanguage = 'ar';
//                     },
//                     child: Text("ar")),
//               ),
//               Container(
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(15),
//                   color: Get.locale!.languageCode == 'en'
//                       ? Colors.amber
//                       : Colors.transparent,
//                 ),
//                 child: TextButton(
//                     onPressed: () {
//                       _languageController.changeLanguage = 'en';
//                     },
//                     child: Text("en")),
//               )
//             ],