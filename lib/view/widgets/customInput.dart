import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../../app/controller/AddOrderController.dart';
import '../../../app/controller/USerController.dart';
import '../../../app/settings/Themes.dart';
import '../../../main.dart';

class CustomInput extends StatelessWidget {
  CustomInput({
    super.key,
    this.controller,
    required this.hint,
    required this.title,
    required this.addOrderController,
    this.control,
  });
  CustomInput.user(
      {super.key,
      required this.controller,
      required this.hint,
      required this.title,
      this.addOrderController,
      this.control});

  final String hint;
  final String title;
  final UserController? controller;
  final TextEditingController? control;
  final AddOrderController? addOrderController;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Expanded(
        //   flex: 1,
        //   child: Text(
        //     title.tr,
        //     style: Theme.of(context)
        //         .textTheme
        //         .bodyLarge!
        //         .copyWith(fontSize: media(context).width * 0.03),
        //   ),
        // ),
        Expanded(
          flex: 3,
          child: SizedBox(
            height: media(context).height * 0.08,
            child: TextFormField(
              inputFormatters: (title == 'phoneNumber' ||
                      title == 'Weight' ||
                      title == "age")
                  ? <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                    ]
                  : [],
              validator: (value) {
                if (title != 'Weight') {
                  if (value!.length < 2) {
                    return "too short input";
                  }
                }
                return null;
              },
              onChanged: controller == null
                  ? (value) {
                      addOrderController!.newOrder.update(
                        title,
                        (_) => value,
                        ifAbsent: () => title,
                      );
                    }
                  : (value) {
                      controller!.newUser.update(
                        title,
                        (_) => value,
                        ifAbsent: () => title,
                      );
                    },
              controller: control,
              textAlign: TextAlign.start,
              textAlignVertical: TextAlignVertical.center,
              decoration: InputDecoration(
                  hintText: hint,
                  hintStyle: textStyle(
                      size: media(context).width * 0.03,
                      color: Colors.black45,
                      weight: FontWeight.normal),
                  alignLabelWithHint: true,
                  focusedBorder: const UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.indigo)),
                  // enabledBorder: OutlineInputBorder(
                  //     borderSide: BorderSide(color: Colors.black)),
                  border: const UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.indigo))),
            ),
          ),
        )
      ],
    );
  }
}
