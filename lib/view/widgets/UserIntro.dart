import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../app/controller/USerController.dart';
import '../../../app/settings/Themes.dart';

class UserIntro extends StatelessWidget {
  const UserIntro({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
            child: SizedBox(
              width: media(context).width * 0.5,
              height: 25,
              child: FittedBox(
                fit: BoxFit.contain,
                alignment: Alignment.center,
                child: Text(
                  'intro'.tr,
                  style: textStyle(
                      size: 25, color: Colors.white, weight: FontWeight.normal),
                ),
              ),
            ),
          ),
          GetBuilder<UserController>(builder: (_) {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
              child: SizedBox(
                width: media(context).width * 0.5,
                height: 25,
                child: FittedBox(
                  alignment: Alignment.center,
                  child: Text(
                    '${UserController.instance.user == null ? 'waiting..' : UserController.instance.user!.name}',
                    style: textStyle(
                        size: 25, color: Colors.white, weight: FontWeight.bold),
                  ),
                ),
              ),
            );
          }),
        ]),
        GetBuilder<UserController>(builder: (_) {
          return Padding(
              padding: const EdgeInsets.all(20.0),
              child: CircleAvatar(
                backgroundImage: AssetImage(
                    'assets/${UserController.instance.user == null ? 'batman.png' : UserController.instance.user!.avatar}'),
                radius: 28,
                backgroundColor: Colors.white,
              ));
        }),
      ]),
    );
  }
}
