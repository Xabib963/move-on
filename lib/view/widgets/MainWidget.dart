import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../app/settings/Themes.dart';
import '../AddOrders/pages/AddOrder.dart';

class MainWidget extends StatelessWidget {
  const MainWidget(
      {super.key, required this.msg, required this.icon, required this.args});

  final String msg;
  final String icon;
  final String args;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(AddOrdersScreen.routeName, arguments: args);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          padding: const EdgeInsets.all(10),
          width: media(context).width * 0.85,
          height: media(context).width * 0.2,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: Colors.indigo.shade300)),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            CircleAvatar(
              backgroundColor: const Color.fromRGBO(197, 208, 250, 1),
              radius: media(context).width * 0.08,
              child: SvgPicture.asset('assets/$icon',
                  width: media(context).width * 0.1, color: Colors.indigo),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SizedBox(
                  width: media(context).width * 0.55,
                  child: FittedBox(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      maxLines: 2,
                      msg,
                      style: textStyle(
                          size: 20,
                          color: Colors.grey[600]!,
                          weight: FontWeight.normal),
                    ),
                  ),
                ),
              ),
            ),
            const Icon(
              Icons.arrow_forward_ios_sharp,
              color: Colors.indigo,
            )
          ]),
        ),
      ),
    );
  }
}
