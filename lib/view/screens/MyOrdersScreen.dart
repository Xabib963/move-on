import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:moveon/app/controller/AddOrderController.dart';

import 'package:moveon/app/settings/Themes.dart';

import '../../../app/controller/USerController.dart';

class MyOrdersScreen extends StatelessWidget {
  MyOrdersScreen({super.key});
  static const routeName = '/myOrders';
  AddOrderController _orderListController = Get.find();
  bool mORc = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo.shade100,
      body: CustomScrollView(slivers: [
        SliverAppBar(
            backgroundColor: const Color.fromRGBO(106, 91, 226, 1),
            centerTitle: true,
            pinned: true,
            floating: false,
            expandedHeight: 200,
            title: Text('O R D E R S'),
            flexibleSpace: FlexibleSpaceBar(
              background: SizedBox(
                  child: Stack(alignment: Alignment.center, children: [
                Positioned(
                  bottom: 5,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white,
                    ),
                    width: 280,
                    height: 80,
                  ),
                ),
                Image.asset(
                  'assets/${UserController.instance.user == null ? 'batman.png' : UserController.instance.user!.avatar}',
                ),
              ])),
            ),
            leading: IconButton(
              icon: Get.locale!.languageCode == 'ar'
                  ? const Icon(Icons.arrow_back_ios, color: Colors.white)
                  : const Icon(Icons.arrow_back_ios_new, color: Colors.white),
              onPressed: () {
                Get.back();
              },
            )),
        GetBuilder<AddOrderController>(
            builder: (_) => _orderListController.orders.isNotEmpty
                ? SliverList(
                    delegate: SliverChildBuilderDelegate(
                        childCount: _orderListController.orders.length,
                        (context, index) {
                    return InkWell(
                      onTap: () {
                        Get.dialog(Container(
                          color: Colors.white,
                          margin: EdgeInsets.symmetric(
                              vertical: media(context).height * 0.25,
                              horizontal: media(context).width * 0.1),
                          child: Scaffold(
                            body: SingleChildScrollView(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: media(context).height * 0.031,
                                      width: double.infinity,
                                    ),
                                    Text(
                                      "Details",
                                      style: textStyle(
                                          size: 22,
                                          color: Colors.indigo,
                                          weight: FontWeight.bold),
                                    ),
                                    _orderListController.orders[index].status ==
                                            'Pinding'
                                        ? Padding(
                                            padding: const EdgeInsets.all(15.0),
                                            child: SingleChildScrollView(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "Pindings",
                                                    style: textStyle(
                                                        size: 25,
                                                        color: Colors.indigo,
                                                        weight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        media(context).height *
                                                            0.031,
                                                    width: double.infinity,
                                                  ),
                                                  Text(
                                                    "Please Wait until your request approved",
                                                    style: textStyle(
                                                        size: 15,
                                                        color: Colors.grey,
                                                        weight:
                                                            FontWeight.bold),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 10,
                                                        horizontal: 20),
                                                    child: Text(
                                                      "• when request approved you will get an phone number",
                                                      style: textStyle(
                                                          size: 15,
                                                          color: Colors.grey,
                                                          weight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 10,
                                                        horizontal: 20),
                                                    child: Text(
                                                      "• use the phone number to make deal with our stuff",
                                                      style: textStyle(
                                                          size: 15,
                                                          color: Colors.grey,
                                                          weight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )
                                        : _orderListController
                                                    .orders[index].status ==
                                                'rejected'
                                            ? Text(
                                                "Sorry we cant take your orders",
                                                style: textStyle(
                                                    size: 15,
                                                    color: Colors.red,
                                                    weight: FontWeight.bold),
                                              )
                                            : Padding(
                                                padding:
                                                    const EdgeInsets.all(15.0),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceAround,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      "Accepted",
                                                      style: textStyle(
                                                          size: 25,
                                                          color: Colors.green,
                                                          weight:
                                                              FontWeight.bold),
                                                    ),
                                                    SizedBox(
                                                      height: media(context)
                                                              .height *
                                                          0.031,
                                                      width: double.infinity,
                                                    ),
                                                    Text(
                                                      "your request has approved",
                                                      style: textStyle(
                                                          size: 20,
                                                          color: Colors.grey,
                                                          weight:
                                                              FontWeight.bold),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 10,
                                                          horizontal: 20),
                                                      child: Text(
                                                        _orderListController
                                                            .orders[index]
                                                            .stuffNumber!,
                                                        style: textStyle(
                                                            size: 22,
                                                            color: Colors.grey,
                                                            weight: FontWeight
                                                                .bold),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 10,
                                                          horizontal: 20),
                                                      child: Text(
                                                        "• use the phone number to make deal with our stuff",
                                                        style: textStyle(
                                                            size: 15,
                                                            color: Colors.grey,
                                                            weight: FontWeight
                                                                .bold),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                  ]),
                            ),
                            backgroundColor: Colors.transparent,
                          ),
                        ));
                      },
                      borderRadius: BorderRadius.circular(25),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  image: DecorationImage(
                                      opacity: 0.1,
                                      fit: BoxFit.contain,
                                      alignment:
                                          Get.locale!.languageCode == 'ar'
                                              ? Alignment.centerLeft
                                              : Alignment.centerRight,
                                      image: const AssetImage(
                                        'assets/one-finger-tap-gesture-of-outlined-hand-symbol-1_icon-icons.com_57982.png',
                                      ))),
                              height: !mORc
                                  ? media(context).height * 0.2
                                  : media(context).height * 0.3,
                              child: Column(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 12.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          SizedBox(
                                            width: media(context).width * 0.9,
                                            height: 30,
                                            child: FittedBox(
                                              alignment:
                                                  Get.locale!.languageCode ==
                                                          'ar'
                                                      ? Alignment.centerRight
                                                      : Alignment.centerLeft,
                                              child: RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'Address'.tr,
                                                    style: textStyle(
                                                        size: 10,
                                                        weight:
                                                            FontWeight.normal,
                                                        color: Colors.black54),
                                                  ),
                                                  TextSpan(
                                                      text:
                                                          '${_orderListController.orders[index].address}         ',
                                                      style: textStyle(
                                                          size: 10,
                                                          weight:
                                                              FontWeight.normal,
                                                          color: Colors.indigo))
                                                ]),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: media(context).width * 0.9,
                                            height: 30,
                                            child: FittedBox(
                                              alignment:
                                                  Get.locale!.languageCode ==
                                                          'ar'
                                                      ? Alignment.centerRight
                                                      : Alignment.centerLeft,
                                              child: RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'date'.tr + ' : ',
                                                    style: textStyle(
                                                        size: 10,
                                                        weight:
                                                            FontWeight.normal,
                                                        color: Colors.black54),
                                                  ),
                                                  TextSpan(
                                                      text: DateFormat.yMMMEd()
                                                          .format(DateTime.parse(
                                                              _orderListController
                                                                  .orders[index]
                                                                  .date!)),
                                                      style: textStyle(
                                                          size: 10,
                                                          weight:
                                                              FontWeight.normal,
                                                          color: Colors.indigo))
                                                ]),
                                              ),
                                            ),
                                          ),
                                          Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                SizedBox(
                                                  height: 25,
                                                  width: media(context).width *
                                                      0.3,
                                                  child: FittedBox(
                                                    child: Text(
                                                      ' ${_orderListController.orders[index].phoneNumber}',
                                                      style: TextStyle(
                                                          color:
                                                              Colors.black54),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: media(context).width *
                                                      0.35,
                                                  height: 28,
                                                  child: FittedBox(
                                                    child: RichText(
                                                      text: TextSpan(children: [
                                                        TextSpan(
                                                          text: 'Weight'.tr +
                                                              ' : ',
                                                          style: textStyle(
                                                              size: 10,
                                                              weight: FontWeight
                                                                  .normal,
                                                              color: Colors
                                                                  .black54),
                                                        ),
                                                        TextSpan(
                                                          text:
                                                              '${_orderListController.orders[index].weight} ',
                                                          style: textStyle(
                                                              size: 10,
                                                              weight: FontWeight
                                                                  .normal,
                                                              color:
                                                                  Colors.black),
                                                        )
                                                      ]),
                                                    ),
                                                  ),
                                                )
                                              ]),
                                          const SizedBox(
                                            width: 0,
                                            height: 0,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Divider(),
                                  Expanded(
                                    flex: 1,
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          SizedBox(
                                            width: _orderListController
                                                        .orders[index]
                                                        .typeOfjourny ==
                                                    'localy'
                                                ? media(context).width * 0.15
                                                : media(context).width * 0.3,
                                            child: FittedBox(
                                              child: Text(
                                                ' ${_orderListController.orders[index].typeOfjourny}'
                                                    .tr,
                                                style: TextStyle(
                                                    color: Colors.black54),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: media(context).width * 0.35,
                                            child: FittedBox(
                                              child: RichText(
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'status'.tr,
                                                    style: textStyle(
                                                        size: 10,
                                                        weight:
                                                            FontWeight.normal,
                                                        color: Colors.black54),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        ' : ${_orderListController.orders[index].status} ',
                                                    style: textStyle(
                                                        size: 10,
                                                        weight:
                                                            FontWeight.normal,
                                                        color: _orderListController
                                                                    .orders[
                                                                        index]
                                                                    .status ==
                                                                'Pinding'
                                                            ? Colors.black54
                                                            : _orderListController
                                                                        .orders[
                                                                            index]
                                                                        .status ==
                                                                    'Accepted'
                                                                ? Colors.green
                                                                : Colors.red),
                                                  )
                                                ]),
                                              ),
                                            ),
                                          )
                                        ]),
                                  ),
                                ],
                              ),
                            )),
                      ),
                    );
                  }))
                : SliverToBoxAdapter(
                    child: Padding(
                        padding:
                            EdgeInsets.only(top: media(context).height * 0.3),
                        child: GetBuilder<AddOrderController>(builder: (_) {
                          return _orderListController.empty
                              ? Center(
                                  child: Text(
                                  'NO oreders Yet',
                                  style: textStyle(
                                      size: 20,
                                      color: Colors.grey,
                                      weight: FontWeight.normal),
                                ))
                              : const SpinKitCircle(
                                  color: Colors.indigo,
                                );
                        })),
                  ))
      ]),
    );
  }
}
