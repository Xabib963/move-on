import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:moveon/app/settings/Themes.dart';

import '../../../app/controller/languagesController.dart';
import '../widgets/CustomAppBar.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../widgets/MainWidget.dart';
import '../widgets/SurveyWidget.dart';
import '../widgets/UserIntro.dart';
import 'MyOrdersScreen.dart';

class HomePage extends StatelessWidget {
  HomePage({
    super.key,
  });
  static const routeName = '/home';
  LanguageController _languageController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const UserIntro(),
            InkWell(
              onTap: () {
                Get.toNamed(MyOrdersScreen.routeName, arguments: false);
              },
              child: Padding(
                padding: const EdgeInsets.only(
                  right: 20.0,
                  left: 20.0,
                  top: 30,
                  bottom: 20.0,
                ),
                child: Align(
                  alignment: Get.locale!.languageCode == 'ar'
                      ? Alignment.centerRight
                      : Alignment.centerLeft,
                  heightFactor: 0.1,
                  child: Text(
                    'my order'.tr,
                    style: textStyle(
                        size: 20,
                        color: Colors.amberAccent,
                        weight: FontWeight.normal),
                  ),
                ),
              ),
            ),
            const SurveyWidget(),
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: SizedBox(
                height: 25,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: media(context).width * 0.3,
                      child: FittedBox(
                        fit: BoxFit.contain,
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Arabic',
                          style: textStyle(
                              size: 20,
                              color: Colors.white54,
                              weight: FontWeight.normal),
                        ),
                      ),
                    ),
                    GetBuilder<LanguageController>(builder: (con) {
                      return Switch(
                        activeColor: Colors.white,
                        inactiveTrackColor: Colors.white38,
                        inactiveThumbColor: Colors.white,
                        value: Get.locale!.languageCode == 'ar' ? false : true,
                        onChanged: (value) {
                          _languageController.changeLanguage =
                              value ? 'en' : 'ar';
                        },
                      );
                    }),
                    SizedBox(
                      width: media(context).width * 0.3,
                      child: FittedBox(
                        fit: BoxFit.contain,
                        alignment: Alignment.centerRight,
                        child: Text(
                          textAlign: TextAlign.start,
                          'English',
                          style: textStyle(
                              size: 20,
                              color: Colors.white54,
                              weight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Card(
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(50))),
              margin: EdgeInsets.zero,
              child: SizedBox(
                height: media(context).height * 0.5,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Divider(
                          endIndent: media(context).width * 0.3,
                          color: Colors.black38,
                          thickness: 3,
                          indent: media(context).width * 0.3,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 10.0),
                        child: Text(
                          'des'.tr,
                          style: textStyle(
                              size: media(context).width * 0.035,
                              color: Colors.grey[800]!,
                              weight: FontWeight.bold),
                        ),
                      ),
                      MainWidget(
                        icon: 'truck_front_transport_vehicle_icon_123464.svg',
                        msg: 'goal1'.tr,
                        args: 'heavy',
                      ),
                      MainWidget(
                        icon: 'two_cars_icon_177210.svg',
                        msg: 'goal2'.tr,
                        args: 'meduim',
                      ),
                      MainWidget(
                        args: 'lite',
                        icon:
                            '3615755-box-delivery-express-parcel-postman-scooter-service_107909.svg',
                        msg: 'goal3'.tr,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}





// Stack(
//               alignment: AlignmentDirectional.centerStart,
//               clipBehavior: Clip.none,
//               children: [
//                 Positioned(
//                     left: position ? media(context).width * 0.22 : null,
//                     right: position ? null : media(context).width * 0.22,
//                     width: media(context).width * 0.7,
//                     height: media(context).height * 0.12,
//                     child: Card(
//                       elevation: 20,
//                       color: color.withOpacity(0.5),
//                       child: Center(
//                         child: Padding(
//                           padding: EdgeInsets.only(
//                               left: position ? 30.0 : 0,
//                               right: position ? 0 : 30.0),
//                           child: Text(
//                             msg,
//                             style: textStyle(
//                                 size: 20,
//                                 color: Colors.white,
//                                 weight: FontWeight.normal),
//                           ),
//                         ),
//                       ),
//                     )),
//                 Positioned(
//                     left: position ? -media(context).width * 0.025 : null,
//                     right: position ? null : -media(context).width * 0.025,
//                     child: CircleAvatar(
//                       backgroundColor: color,
//                       radius: media(context).width * 0.155,
//                       child: SvgPicture.asset('assets/$icon',
//                           width: media(context).width * 0.2,
//                           color: Colors.white),
//                     ))
//               ]),