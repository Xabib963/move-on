import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../data/model/Order.dart';
import 'AuthController.dart';
import 'USerController.dart';

class AddOrderController extends GetxController {
  var newOrder = {}.obs;
  String journy = 'localy';
  void updateJourny(value) {
    journy = value;

    update();
  }

  var orders = <Order>[].obs;
  // String? Address;
  // Future<Placemark> GetAddressFromLatLong(
  //     double latitude, double longitude) async {
  //   List<Placemark> placemarks =
  //       await placemarkFromCoordinates(latitude, longitude);
  //   print(placemarks);
  //   Placemark place = placemarks[0];
  //   Address =
  //       '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
  //   return place;
  // }
  var empty = false;
  Future<List<Order>> getOrder(use) async {
    String url = use == 'empl'
        ? 'https://m1.firebasedatabase.app/orders.json?auth=${AuthController.instance.token}&orderBy="creatorId"&equalTo="${AuthController.instance.userId}"'
        : 'https://op/orders.json?auth=${AuthController.instance.token}';
    var response = await http.get(Uri.parse(url));
    orders.clear();
    (json.decode(response.body) as Map<String, dynamic>?)?.forEach(
      (key, value) {
        orders.add(Order.fromJson(value, key));
      },
    );
    if (orders.isEmpty) {
      empty = true;
      update();
    }
    update();
    return orders;
  }

  Future<void> updateStatus(String id, String value, String snum) async {
    var body = jsonEncode({'status': value, 'stuffNumber': snum});

    var response = await http.patch(
        Uri.parse(
            'hebasedatabase.app/orders/$id.json?auth=${AuthController.instance.token}'),
        body: body);

    if (response.statusCode == 200) {
      var results = jsonDecode(response.body);
      getOrder(UserController.instance.user!.role);
    } else {
      throw Exception();
    }
  }

  @override
  void onInit() async {
    await getOrder(UserController.instance.user!.role);
    super.onInit();
  }

  String date = DateFormat.yMMMEd().format(DateTime.now());
  void chooseDate(BuildContext context) async {
    DateTime? selectedDate = await showDatePicker(
        initialDatePickerMode: DatePickerMode.day,
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2085));
    String timeFormated = DateFormat.yMMMd().format(DateTime.now());

    if (selectedDate == null) {
      return null;
    } else {
      timeFormated = DateFormat.yMMMd().format(selectedDate!);
    }

    date = timeFormated;
    update();
    debugPrint('############### ERROR #####################');
  }

  Future<void> addOrder({Map? newOrdere}) async {
    var body = jsonEncode(newOrdere);

    var response = await http.post(
        Uri.parse(
            'https:/abase.app/orders.json?auth=${AuthController.instance.token}'),
        body: body);

    if (response.statusCode == 200) {
      var results = jsonDecode(response.body);
    } else {
      throw Exception();
    }
  }
}
