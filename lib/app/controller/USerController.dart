import 'dart:convert';

import 'package:get/get.dart';
import 'package:moveon/view/Screens/HomePage.dart';
import 'package:moveon/view/Screens/MyOrdersScreen.dart';
import '../../data/model/UserInfo.dart';
import '../controller/AuthController.dart';
import 'package:http/http.dart' as http;

class UserController extends GetxController {
  static UserController instance = Get.find();
  int index = 0;
  String avatar = '';
  void updateIndex(int n, String ava) {
    index = n;
    avatar = ava;
    update();
  }

  UserInfo? user;
  Future<UserInfo> getUser() async {
    var response = await http.get(Uri.parse(
        'https://se.app/users.json?auth=${AuthController.instance.token}&orderBy="userId"&equalTo="${AuthController.instance.userId}"'));
    print(json.decode(response.body));
    if (response.statusCode == 200) {
      UserInfo user =
          UserInfo.fromJson((json.decode(response.body))?.values.first);
      user.role == 'empl'
          ? Get.toNamed(HomePage.routeName)
          : Get.toNamed(MyOrdersScreen.routeName, arguments: true);
      return user;
    } else {
      throw Exception();
    }
  }

  var newUser = {}.obs;

  void assignUser() async {
    user = await getUser();
    AuthController.instance.logupdate();
    update();
  }
}
