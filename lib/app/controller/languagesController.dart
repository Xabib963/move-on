import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LanguageController extends GetxController {
  set changeLanguage(String lang) {
    Locale locale = new Locale(lang);
    Get.updateLocale(locale);
    update();
  }
}
