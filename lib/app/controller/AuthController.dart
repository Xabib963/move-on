import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:moveon/view/Screens/HomePage.dart';

import 'USerController.dart';

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  // late Rx<User?> _user;
  bool loading = false;
  String? _token;
  DateTime? _expiryDate;
  String? _userId;

  Timer? _authTimer;

  bool get isAuth {
    return token != null;
  }

  navigate() {
    isAuth
        ? {Get.put(UserController()), UserController.instance.assignUser()}
        : null;
  }

  Future<void> addUser({Map<String, dynamic>? newUser}) async {
    var body = jsonEncode(newUser);

    var response = await http.post(
        Uri.parse(
            'h.app/users.json?auth=${AuthController.instance.token}'),
        body: body);

    if (response.statusCode == 200) {
      var results = jsonDecode(response.body);
    } else {
      throw Exception();
    }
  }

  String? get token {
    if (_expiryDate != null &&
        _expiryDate!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  String? get userId {
    return _userId;
  }

  void logupdate() {
    loading = !loading;
    update();
  }

  // User? get user => auth.currentUser;
  // FirebaseAuth auth = FirebaseAuth.instance;
  bool sighnUp = false;
  void changToSignUp() {
    sighnUp = !sighnUp;
    update();
  }

  // @override
  // void onReady() {
  //   super.onReady();

  //   _user = Rx<User?>(auth.currentUser);
  //   _user.bindStream(auth.userChanges());
  //   ever(_user, initialScreen);
  // }

  // initialScreen(User? user) {
  //   if (user == null) {
  //     Get.offAllNamed(LoginsScreen.routeName);
  //   } else {
  //     Get.offAllNamed(MyHomePage.routeName);
  //   }
  // }

  // void loginwithemail({required String email, required String password}) async {
  //   try {
  //     await auth.signInWithEmailAndPassword(email: email, password: password);
  //   } on FirebaseAuthException catch (e) {
  //     if (e.code == 'user-not-found') {
  //       Get.snackbar("user-not-found", "Try Again",
  //           backgroundColor: Colors.red.withOpacity(0.7));
  //     } else if (e.code == 'wrong-password') {
  //       Get.snackbar("wrong-password", "Try Again",
  //           backgroundColor: Colors.red.withOpacity(0.7));
  //     }
  //   }
  // }
  //  Future<void> authinticate({required String email,required String password,required String urlSegment})async{

  // const url = "https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=";

  //   try {
  //     final response = await http.post(Uri.parse(url),
  //         body: json.encode({
  //           'email': email,
  //           'password': password,
  //           'returnSecureToken': true
  //         }));

  //   }catch(e){
  //     print(e);
  //   };
  //  }
  Future<void> register(
      {required String email, required String password}) async {
    const url =
        "https4";
    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true
          }));
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      _token = responseData['idToken'];
      _userId = responseData['localId'];

      print(_userId);
      _expiryDate = DateTime.now().add(Duration(
        seconds: int.parse(
          responseData['expiresIn'],
        ),
      ));
    } catch (e) {
      Get.snackbar("Try Again", "Sign Up failed", backgroundColor: Colors.red);

      print(e);
    }
  }

  Future<void> login({required String email, required String password}) async {
    const url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzL4";

    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true
          }));
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        if (responseData['error']['message']
            .toString()
            .contains("EMAIL_NOT_FOUND")) {
          Get.snackbar("EMAIL NOT FOUND", "Sorry");
        }
        if (responseData['error']['message']
            .toString()
            .contains("INVALID_PASSWORD")) {
          Get.snackbar("INVALID PASSWORD", "Sorry");
        }
        if (responseData['error']['message']
            .toString()
            .contains("USER_DISABLED")) {
          Get.snackbar("USER DISABLED", "Sorry");
        }
      }

      _token = responseData['idToken'];
      _userId = responseData['localId'];
      print('ssssssssssss');
      print(_userId);
      _expiryDate = DateTime.now().add(Duration(
        seconds: int.parse(
          responseData['expiresIn'],
        ),
      ));
    } catch (e) {}
  }
}
