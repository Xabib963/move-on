import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../data/model/direction.dart';

class MapsController extends GetxController {
  late Position _position;
  Directions? info;
  Marker? origin;
  Marker? destination;
  static const String _baseUrl =
      'https://maps.googleapis.com/maps/api/directions/json?';

  Set<Marker> _markers = {};

  Set<Marker> get marker => _markers;
  Future<Position> getLocation() async {
    bool _serviceEnabled;

    LocationPermission _permissionGranted;

    _serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await Geolocator.openLocationSettings();
    }
    if (!_serviceEnabled) {
      throw Exception();
    }
    _permissionGranted = await Geolocator.checkPermission();
    if (_permissionGranted == LocationPermission.denied) {
      _permissionGranted = await Geolocator.requestPermission();
    }
    if (_permissionGranted == LocationPermission.deniedForever) {
      throw Exception();
    }
    _position = await Geolocator.getCurrentPosition();
    return _position;
  }

  BitmapDescriptor pinIcon = BitmapDescriptor.defaultMarkerWithHue(0.5);
  void setPinicon() {
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(10, 10)),
            'assets/1486164730-114_79711.png')
        .then((value) => pinIcon = value);
  }

  String? mapStyle;
  void setStyle() {
    rootBundle.loadString('assets/style/map_style.txt').then((string) {
      mapStyle = string;
    });
  }

  Future<void> setMarker(Marker mark, bool o) async {
    if (o) {
      origin = mark;
      _markers.add(origin!);
      update();
    } else {
      destination = mark;
      _markers.add(destination!);
      update();
    }
  }

  // Dio dio = Dio();
  // Future<Directions> getDirections({
  //   required LatLng origin,
  //   required LatLng dest,
  // }) async {
  //   try {
  //     final response = await dio.get(_baseUrl, queryParameters: {
  //       'origin': '${origin.latitude},${origin.longitude}',
  //       'destination': '${dest.latitude},${dest.longitude}',
  //       'key': "Api Key"
  //     });
  //     print(response.data);
  //     return Directions.fromMap(response.data);
  //   } catch (e) {
  //     throw Exception(e);
  //   }
  //   //Check if response is successfull
  // }

  // setDirections() async {
  //   info = await getDirections(
  //       origin: origin!.position, dest: destination!.position);
  //   update();
  // }

  @override
  void onInit() async {
    setPinicon();
    getLocation();
    setStyle();

    super.onInit();
  }
}
