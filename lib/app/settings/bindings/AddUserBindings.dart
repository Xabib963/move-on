import 'package:get/get.dart';

import '../../controller/USerController.dart';

class AddUserBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(UserController(), permanent: true);
  }
}
