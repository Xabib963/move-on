import 'package:get/get.dart';
import 'package:moveon/app/controller/MapsController.dart';

class MapBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(MapsController());
  }
}
