import 'package:get/get.dart';

import '../../controller/AddOrderController.dart';

class AddOdrederBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(AddOrderController());
  }
}
