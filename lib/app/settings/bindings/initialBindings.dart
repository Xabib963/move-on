import 'package:get/get.dart';
import 'package:moveon/app/controller/AuthController.dart';
import 'package:moveon/app/controller/USerController.dart';
import 'package:moveon/app/controller/languagesController.dart';

class InitialBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(LanguageController());
    Get.put(AuthController());
  }
}
