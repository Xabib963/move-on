import 'package:get/get.dart';
import 'package:moveon/app/controller/MapsController.dart';

import 'package:moveon/app/settings/bindings/addOrderBindings.dart';
import 'package:moveon/app/settings/bindings/mapBindings.dart';

import 'package:moveon/view/AuthScreen/pages/Addinformation.dart';

import 'package:moveon/view/AuthScreen/pages/LoginScreen.dart';

import '../../view/AddOrders/pages/AddOrder.dart';
import '../../view/AddOrders/pages/MapScreen.dart';
import '../../view/Screens/HomePage.dart';
import '../../view/Screens/MyOrdersScreen.dart';
import 'bindings/AddUserBindings.dart';
import 'bindings/initialBindings.dart';

appRoutes() => [
      GetPage(
        name: LoginsScreen.routeName,
        page: () => LoginsScreen(),
        binding: InitialBindings(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: MyOrdersScreen.routeName,
        page: () => MyOrdersScreen(),
        binding: AddOdrederBindings(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: AdduserInfo.routeName,
        page: () => AdduserInfo(),
        binding: AddUserBindings(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: HomePage.routeName,
        page: () => HomePage(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: AddOrdersScreen.routeName,
        page: () => AddOrdersScreen(),
        binding: AddOdrederBindings(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: MapScreen.routeName,
        page: () => MapScreen(),
        binding: MapBindings(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
    ];
