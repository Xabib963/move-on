import 'package:flutter/material.dart';

TextStyle textStyle(
        {required double size,
        required Color color,
        required FontWeight weight}) =>
    TextStyle(
      fontWeight: weight,
      fontSize: size,
      color: color,
    );

Size media(BuildContext context) => MediaQuery.of(context).size;
