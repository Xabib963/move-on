class Order {
  String? typeOfjourny;
  String? phoneNumber;
  String? weight;
  String? fullName;
  String? address;
  String? slatitude;
  String? slongitude;
  String? dlatitude;
  String? dlongitude;
  String? date;
  String? id;
  String? status;
  String? city;
  String? stuffNumber;
  Order(
      {this.typeOfjourny,
      this.phoneNumber,
      this.weight,
      this.fullName,
      this.address,
      this.slatitude,
      this.slongitude,
      this.dlatitude,
      this.dlongitude,
      this.date,
      this.stuffNumber,
      this.city});

  Order.fromJson(Map<String, dynamic> json, String id) {
    typeOfjourny = json['typeOfjourny'] as String;
    phoneNumber = json['phoneNumber'].toString();
    weight = json['Weight'].toString();
    fullName = json['fullName'] as String;
    address = json['Address'] as String;
    slatitude = json['Slatitude'].toString();
    slongitude = json['Slongitude'].toString();
    dlatitude = json['dlatitude'].toString();
    dlongitude = json['dlongitude'].toString();
    date = json['date'] as String;
    this.id = id;
    city = json['city'];
    status = json['status'] as String;
    stuffNumber = json['stuffNumber'] as String;
  }
}
