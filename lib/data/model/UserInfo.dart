class UserInfo {
  String? email;
  String? name;
  String? address;
  String? age;
  String? job;
  String? number;
  String? id;
  String? avatar;
  String? role;
  UserInfo(
      {this.email,
      this.name,
      this.role,
      this.address,
      this.age,
      this.job,
      this.number,
      this.avatar});

  UserInfo.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['Name'];
    address = json['address'];
    age = json['age'];
    job = json['job'];
    number = json['number'];
    avatar = json['avatar'];
    role = json['role'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['Name'] = this.name;
    data['address'] = this.address;
    data['age'] = this.age;
    data['job'] = this.job;
    data['number'] = this.number;
    data['avatar'] = this.avatar;
    return data;
  }
}
